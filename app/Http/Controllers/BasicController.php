<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\sendingContactEmail;



class BasicController extends Controller
{
    public function about()
    {
        return view('about');
    }

    public function index()
    {
        return view('index');
    }

    public function course()
    {
        return view('courses');
    }

    public function contact()
    {
        return view('contact');
    }

    public function storeContact(Request $request)
    {
        $validatedData = $request->validate(
            [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required',
                'phone_number' => 'required|max:255',
                'message' => 'required',
            ]

        );

        Contact::insert([
            'first_name' => $request->first_name,
            'last_name' => $request->first_name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'message' => $request->message,
            'created_at' => Carbon::now()
        ]);

        $data = [
            'first_name' => $request->first_name,
            'last_name' => $request->first_name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'message' => $request->message,
            'created_at' => Carbon::now()
        ];

        // Mail::to($request->email)->send(new sendingContactEmail($data));
        return redirect()->back()->with('success', 'Thank You !!! Shall Get Back To You Soon....');
    }
}