@extends('layout.index')
@section('content')
<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">About Us</h2>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <section class="section-padding">
        <div class="container">
            <div class="row showcase-section">
                <div class="col-md-6">
                    <img src="img/dev1.png" alt="showcase image">
                </div>
                <div class="col-md-6">
                    <div class="about-text">
                        <h3>About us:</h3>
                        <p style="font-size: 21px !important;">
                        Welcome to the World of IQ Trainings!!!
                        AIM : Silhouette your future with us…
                        Our education system is churning out tens of thousands of graduates in various disciplines 
                        every year. Students complete their graduation with the hope of a bright future. But, they fail to 
                        understand that merely getting a degree is no longer sufficient to perform and succeed in 
                        today’s competitive world.
                        The sole purpose of IQ Trainings existence is, we act as a bridge between our students and 
                        their bright future in companies by imparting them with necessary Technical skills, soft skills, 
                        aptitude, and attitude that make them ready to face with confidence the recruitment tests first, 
                        and later the challenges of the professional world.
                        Candidates who have an aforementioned skill are often more confident when applying to certain 
                        industries than those who don't. In addition, employees with a technical skill are often better at 
                        multitasking in a challenging and complex role. With sufficient technical ability, you will be able 
                        to speak to colleagues and clients more confidently using your specialist expertise.
                        Graduates who take the time to learn a technical skill often receive higher pay. Businesses are 
                        always on the lookout for knowledgeable staff, as their clients expect to work with highly skilled 
                        teams who they have confidence in to deliver the results they need.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
</section>
@endsection