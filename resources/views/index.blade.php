@extends('layout.index')
@section('content')

<section id="banner">
    <div id="main-slider" class="flexslider">
        <ul class="slides">
            <li>
                <img src="{{asset('/img/slides/1.jpeg')}}" alt="" />
                <div class="flex-caption">
                    <h3>Python</h3>
                    <!-- <p>We Teach Students for Sucessful Fututre</p> -->

                </div>
            </li>
            <li>
                <img src="{{asset('/img/slides/java3.jpeg')}}" alt="" />
                <div class="flex-caption">
                    <h3>Java</h3>
                    <!-- <p>Lorem ipsum dolo amet, consectetur provident.</p> -->

                </div>
            </li>
            <!-- <li>
                <img src="{{asset('/img/slides/3.jpg')}}" alt="" />
                <div class="flex-caption">
                    <h3>Learn to be Sucessful</h3>
                    <p>Lorem ipsum dolo amet, consectetur provident.</p>

                </div>
            </li> -->
        </ul>
    </div>
</section>

<section id="call-to-action-2">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-9">
                <h3>Welcome to IQ Trainings </h3>
                <p>IQ training provides easy and point to point learning of various online tutorials such as Python, Core Java, Oracle database programming, Data structure and design pattern, SQL, Cloud Computing etc. IQ training is growing day by day with lot of new contents on various software technologies.</p>
            </div>
            <div class="col-md-2 col-sm-3">
                <a href="{{url('/courses')}}" class="btn btn-primary">Read More</a>
            </div>
        </div>
    </div>
</section>




@endsection