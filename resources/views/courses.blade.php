@extends('layout.index')
@section('content')
<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Courses</h2>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container content">
        <!-- Service Blcoks -->

        <div class="row">
            <div class="col-md-12">
                <div class="about-logo">
                    <h3>Our Best <span class="color">COURSES</span></h3>
                    <p>IQ training provides easy and point to point learning of various online tutorials such as Python, Core Java, Oracle database programming, Data structure and design pattern, SQL, Cloud Computing etc. IQ training is growing day by day with lot of new contents on various software technologies.</p>
                </div>
            </div>
        </div>

        <!-- Info Blcoks -->
        <div class="row">
            <div class="col-sm-4 info-blocks">
                <!-- <i class="icon-info-blocks fa fa-bell-o"></i> -->
                <img src="{{asset('/img/Python.png')}}" />
                <div class="info-blocks-in">
                    <h3>Python,Data Structure, Database</h3>
                    <p>Python is an interpreted high-level general-purpose programming language</p>
                    <a href="{{asset('/img/course/IQ_Training_Python_Database.pdf')}}" class="btn btn-primary" download>Download</a>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
            <img src="{{asset('/img/java.jpeg')}}" />
                <div class="info-blocks-in">
                    <h3>Java</h3>
                    <p>Java is a high-level, class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible.</p>
                       
                        <a href="{{asset('/img/course/Java_doc_syllabus-converted.pdf')}}" class="btn btn-primary" download>Download</a>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
            <img src="{{asset('/img/oracle.jpeg')}}" />
                <div class="info-blocks-in">
                    <h3>Oracle</h3>
                    <p>An Oracle database is a collection of data treated as a unit. The purpose of a database is to store and retrieve related information.</p>
                        <a href="{{asset('/img/course/Oracle_syllabus.pdf')}}" class="btn btn-primary" download>Download</a>
                </div>
            </div>
        </div>
        <!-- End Info Blcoks -->


        <!-- Info Blcoks -->
        <div class="row">
            <div class="col-sm-4 info-blocks">
            <img src="{{asset('/img/apptitude.jpeg')}}" />
                <div class="info-blocks-in">
                    <h3>Apptitude</h3>
                    <p>An aptitude test is an exam used to determine an individual's skill or propensity to succeed in a given activity.</p>
                    <a href="{{asset('/img/course/IQ Trainings Aptitude & Soft skill Training Program-converted.pdf')}}" class="btn btn-primary" download>Download</a>
                </div>
            </div>
            <!-- <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-compress"></i>
                <div class="info-blocks-in">
                    <h3>Fully Responsive</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam,
                        incidunt</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-html5"></i>
                <div class="info-blocks-in">
                    <h3>CSS3 + HTML5</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam,
                        incidunt</p>
                </div>
            </div> -->
        </div>
        <!-- End Info Blcoks -->
       
        <!-- End Service Blcoks -->




    </div>
</section>
@endsection