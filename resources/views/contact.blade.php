@extends('layout.index')
@section('content')
<section id="inner-headline">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Contact Us</h2>
            </div>
        </div>
    </div>
</section>
<div class="container">
       
    <div class="wrap-contact100">

       @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                <strong>{{ session()->get('success') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <form class="contact100-form validate-form" action="{{ route('store_contact') }}" method="POST">
            <span class="contact100-form-title">
                Contact Us
            </span>
            @csrf

            <label class="label-input100" for="first-name">Your Name *</label>
            <div class="wrap-input100 rs1 validate-input">
                <input id="first-name" class="input100" type="text" name="first_name" value="{{ old('first_name')}}" placeholder="First name">
                <span class="focus-input100"></span>
                @error('first_name')
                <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>
            <div class="wrap-input100 rs1 validate-input">
                <input class="input100" type="text" name="last_name" value="{{ old('last_name')}}" placeholder="Last name">
                <span class="focus-input100"></span>
                @error('last_name')
                <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>

            <label class="label-input100" for="email">Email Address *</label>
            <div class="wrap-input100 validate-input">
                <input id="email" class="input100" type="text" name="email" value="{{ old('email')}}" placeholder="Eg. example@email.com">
                <span class="focus-input100"></span>
                @error('email')
                <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>

            <label class="label-input100" for="phone">Phone Number</label>
            <div class="wrap-input100">
                <input id="phone" class="input100" type="text" name="phone_number" value="{{ old('phone_number')}}" placeholder="Eg. +1 800 000000">
                <span class="focus-input100"></span>
                @error('phone_number')
                <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>

            <label class="label-input100" for="message">Message *</label>
            <div class="wrap-input100 validate-input">
                <textarea id="message" class="input100" name="message"
                    placeholder="Please enter your comments..."> {{ old('message')}}</textarea>
                <span class="focus-input100"></span>
                @error('message')
                <span class="text-danger"> {{ $message }}</span>
                @enderror
            </div>

            <div class="container-contact100-form-btn">
                <button class="contact100-form-btn">
                    <span>
                        Submit
                        <i class="zmdi zmdi-arrow-right m-l-8"></i>
                    </span>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection