<header>
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}"><img src="img/IQ_png.png" alt="logo" /> &nbsp;IQ Training</a>
            </div>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{url('/')}}">Home</a></li>
                    <!-- <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">About Us <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="about.html">Our Institute</a></li>
                            <li><a href="#">Our Team</a></li>
                            <li><a href="#">News</a></li>
                            <li><a href="#">Investors</a></li>
                        </ul>
                    </li> -->
                    <li class="{{ Request::is('about') ? 'active' : '' }}"><a href="{{url('/about')}}">About Us</a>
                    </li>
                    <li class="{{ Request::is('courses') ? 'active' : '' }}"><a href="{{url('/courses')}}">Courses</a>
                    </li>
                    <!-- <li><a href="portfolio.html">Portfolio</a></li>
                    <li><a href="pricing.html">Fees</a></li> -->
                    <li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="{{url('/contact')}}">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>