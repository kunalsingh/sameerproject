<meta charset="utf-8">
<title>IQ Training</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://webthemez.com" />
<!-- css -->
<link href='{{asset("css/bootstrap.min.css")}}' rel="stylesheet" />
<link href="{{asset('css/fancybox/jquery.fancybox.css')}}" rel="stylesheet">
<link href="{{asset('css/jcarousel.css')}}" rel="stylesheet" />
<link href="{{asset('css/flexslider.css')}}" rel="stylesheet" />
<link href="{{asset('js/owl-carousel/owl.carousel.css')}}" rel="stylesheet">
<link href="{{asset('css/style.css')}}" rel="stylesheet" />
<link href="{{asset('css/main.css')}}" rel="stylesheet" />
<link href="{{asset('css/utils.css')}}" rel="stylesheet" />