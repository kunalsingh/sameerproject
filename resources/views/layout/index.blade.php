<!DOCTYPE html>
<html lang="en">

<head>
    @include('layout.partials.head')
</head>

<body>
    <div id="wrapper" class="home-page">
        @include('layout.partials.topbar')
        <!-- start header -->
        @include('layout.partials.header')
        <!-- end header -->
        <!-- image slider -->


        <!-- end image slider -->
        @yield('content')

        @include('layout.partials.footer')
    </div>

</body>

</html>